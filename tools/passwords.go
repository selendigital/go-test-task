package tools

import "golang.org/x/crypto/bcrypt"

const (
	bcryptPasswordCost = 13
	bcryptTokenCost    = 5
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcryptPasswordCost)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
