package main

import (
	"api/resolvers"
	"log"
	"net/http"

	"github.com/vettich/restik"
)

func main() {
	resolv := resolvers.NewResolver()
	r := restik.NewRouter()
	r.Use(new(resolvers.TokenMiddleware))

	r.Post("/login", resolv.Login)

	r.Get("/my/books", resolv.ListMyBooks)
	r.Get("/books", resolv.ListBooks)
	r.Post("/books", resolv.CreateBook)
	r.Get("/books/{book_id}", resolv.GetBook)
	r.Put("/books/{book_id}", resolv.UpdateBook)
	r.Delete("/books/{book_id}", resolv.DeleteBook)

	log.Fatalln(http.ListenAndServe(":3003", r.Handler()))
}
