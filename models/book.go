package models

import (
	"sort"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/vettich/restik"
)

var (
	BookNotFoundError = restik.NewNotFoundError("Book not found")
)

type Books []Book

type Book struct {
	ID        string    `json:"id"`
	UserID    string    `json:"user_id"`
	Name      string    `json:"name"`
	Year      int       `json:"year"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func NewBook(userID, name string, year int) *Book {
	return &Book{
		uuid.New().String(),
		userID,
		name,
		year,
		time.Now(),
		time.Now(),
	}
}

func (books Books) FindByID(bookID string) (*Book, error) {
	for _, b := range books {
		if b.ID == bookID {
			return &b, nil
		}
	}
	return nil, BookNotFoundError
}

func (books Books) Filter(userID, name string, year int) Books {
	filtered := Books{}
	for _, b := range books {
		pass := true
		if userID != "" && userID != b.UserID {
			pass = false
		}
		if name != "" && strings.Index(b.Name, name) < 0 {
			pass = false
		}
		if year > 0 && b.Year != year {
			pass = false
		}
		if pass {
			filtered = append(filtered, b)
		}
	}
	return filtered
}

func (books Books) Sort(field string) Books {
	sorted := books
	sort.Slice(sorted, func(i, j int) bool {
		if field == "name" || field == "+name" {
			return strings.Compare(sorted[i].Name, sorted[j].Name) < 0
		}
		if field == "-name" {
			return strings.Compare(sorted[i].Name, sorted[j].Name) > 0
		}
		if field == "year" || field == "+year" {
			return sorted[i].Year < sorted[j].Year
		}
		if field == "-year" {
			return sorted[i].Year > sorted[j].Year
		}
		return true
	})
	return sorted
}

func (books *Books) Add(userID, name string, year int) *Book {
	book := NewBook(userID, name, year)
	*books = append(*books, *book)
	return book
}

func (books *Books) Set(book *Book) {
	for i, b := range *books {
		if b.ID == book.ID {
			(*books)[i] = *book
		}
	}
}

func (books *Books) Delete(bookID string) {
	for i, b := range *books {
		if b.ID == bookID {
			(*books) = append((*books)[:i], (*books)[i+1:]...)
			break
		}
	}
}
