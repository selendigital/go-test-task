package models

import (
	"api/tools"
	"errors"

	"github.com/google/uuid"
)

var (
	UserNotFoundError      = errors.New("user not found")
	UserPasswordWrongError = errors.New("user password wrong")
)

type Users []User

type User struct {
	ID           string `json:"id"`
	Name         string `json:"name"`
	Login        string `json:"login"`
	PasswordHash string `json:"-"`
}

func NewUser(id, name, login, password string) *User {
	if id == "" {
		id = uuid.New().String()
	}
	passwordHash, _ := tools.HashPassword(password)
	return &User{
		id,
		name,
		login,
		passwordHash,
	}
}

func (u *User) CreateBook(name string, year int) *Book {
	return NewBook(u.ID, name, year)
}

func (u *User) CheckPassword(password string) bool {
	return tools.CheckPasswordHash(password, u.PasswordHash)
}

func (users Users) CheckUser(login, password string) (*User, error) {
	for _, u := range users {
		if u.Login != login {
			continue
		}
		if !u.CheckPassword(password) {
			return &u, UserPasswordWrongError
		}
		return &u, nil
	}
	return nil, UserNotFoundError
}
