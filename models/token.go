package models

import "github.com/google/uuid"

type Tokens []Token

type Token struct {
	ID     string
	UserID string
}

func NewTokenFromUserID(userID string) *Token {
	return &Token{
		uuid.New().String(),
		userID,
	}
}

func (tokens *Tokens) New(userID string) *Token {
	token := NewTokenFromUserID(userID)
	*tokens = append(*tokens, *token)
	return token
}

func (tokens Tokens) FindByID(tokenID string) *Token {
	for _, t := range tokens {
		if t.ID == tokenID {
			return &t
		}
	}
	return nil
}
