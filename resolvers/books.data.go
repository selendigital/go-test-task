package resolvers

import "api/models"

var books models.Books

func init() {
	books.Add("1", "Book 1", 1222)
	books.Add("1", "Book 2", 2000)
	books.Add("1", "Book 3", 2001)
	books.Add("1", "Gundzila", 2001)
	books.Add("2", "Book 4", 1999)
}
