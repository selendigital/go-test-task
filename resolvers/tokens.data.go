package resolvers

import "api/models"

var tokens models.Tokens

func init() {
	tokens = append(tokens,
		models.Token{
			ID:     "tokenOf1",
			UserID: "1",
		},
		models.Token{
			ID:     "tokenOf2",
			UserID: "2",
		},
	)
}
