package resolvers

type LoginArgs struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

func (r *Resolver) Login(args LoginArgs) (string, error) {
	user, err := users.CheckUser(args.Login, args.Password)
	if err != nil {
		return "", err
	}
	token := tokens.New(user.ID)
	return token.ID, nil
}
