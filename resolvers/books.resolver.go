package resolvers

import (
	"api/models"
	"strconv"
	"time"

	"github.com/vettich/restik"
)

func (r *Resolver) GetBook(req *restik.Request) (*models.Book, error) {
	bookID := req.Vars.String("book_id")
	book, err := books.FindByID(bookID)
	if err != nil {
		return nil, err
	}
	return book, nil
}

type ListBooksReply struct {
	Books models.Books `json:"books"`
	Total int          `json:"total"`
}

func (r *Resolver) ListBooks(req *restik.Request) *ListBooksReply {
	name := req.URL.Query().Get("filter_name")
	year, _ := strconv.Atoi(req.URL.Query().Get("filter_year"))
	filtered := books.Filter("", name, year)
	sorted := filtered.Sort(req.URL.Query().Get("sort"))
	return &ListBooksReply{sorted, len(sorted)}
}

func (r *Resolver) ListMyBooks(req *restik.Request) (*ListBooksReply, error) {
	token, err := getToken(req)
	if err != nil {
		return nil, err
	}
	name := req.URL.Query().Get("filter_name")
	year, _ := strconv.Atoi(req.URL.Query().Get("filter_year"))
	myBooks := books.Filter(token.UserID, name, year)
	sorted := myBooks.Sort(req.URL.Query().Get("sort"))
	return &ListBooksReply{sorted, len(sorted)}, nil
}

type BookInput struct {
	Name string `json:"name"`
	Year int    `json:"year"`
}

func (input BookInput) Check() error {
	if input.Name == "" {
		return restik.NewBadRequestError("book's name is empty")
	}
	return nil
}

func (r *Resolver) CreateBook(req *restik.Request, input BookInput) (bool, error) {
	token, err := getToken(req)
	if err != nil {
		return false, err
	}
	if err := input.Check(); err != nil {
		return false, err
	}
	books.Add(token.UserID, input.Name, input.Year)
	return true, nil
}

func (r *Resolver) UpdateBook(req *restik.Request, input BookInput) (bool, error) {
	token, err := getToken(req)
	if err != nil {
		return false, err
	}
	if err := input.Check(); err != nil {
		return false, err
	}
	book, err := books.FindByID(req.Vars.String("book_id"))
	if err != nil {
		return false, err
	}
	if book.UserID != token.UserID {
		return false, restik.NewError(403, "forbidden", "You cannot update this book")
	}
	book.Name = input.Name
	book.Year = input.Year
	book.UpdatedAt = time.Now()
	books.Set(book)
	return true, nil
}

func (r *Resolver) DeleteBook(req *restik.Request) (bool, error) {
	token, err := getToken(req)
	if err != nil {
		return false, err
	}
	book, err := books.FindByID(req.Vars.String("book_id"))
	if err != nil {
		return false, err
	}
	if book.UserID != token.UserID {
		return false, restik.NewError(403, "forbidden", "You cannot update this book")
	}
	books.Delete(book.ID)
	return true, nil
}
