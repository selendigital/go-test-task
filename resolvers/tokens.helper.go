package resolvers

import (
	"api/models"

	"github.com/vettich/restik"
)

func getToken(req *restik.Request) (*models.Token, error) {
	tokenStr := req.Vars.String("token")
	if tokenStr == "" {
		return nil, restik.NewError(403, "forbidden", "Token not found")
	}
	return tokens.FindByID(tokenStr), nil
}
