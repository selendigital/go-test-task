package resolvers

import "api/models"

var users models.Users

func init() {
	users = append(
		users,
		*models.NewUser("1", "Don", "don", "donpass"),
		*models.NewUser("2", "Jonn", "jonn", "johnpass"),
	)
}
