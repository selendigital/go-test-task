package resolvers

import (
	"strings"

	"github.com/vettich/restik"
)

type TokenMiddleware struct{}

func (mw TokenMiddleware) Middleware(next restik.HandlerFunc) restik.HandlerFunc {
	return func(w restik.ResponseWriter, r *restik.Request) {
		var token string
		if t := r.URL.Query().Get("token"); t != "" {
			token = t
		} else if ah := r.Headers.Get("Authorization"); ah != "" {
			if strings.HasPrefix(ah, "Bearer") {
				token = strings.TrimSpace(ah[len("Bearer"):])
			}
		}
		if token != "" && tokens.FindByID(token) == nil {
			token = ""
		}
		r.Vars["token"] = token
		next(w, r)
	}
}
