module api

go 1.15

require (
	github.com/google/uuid v1.1.2
	github.com/vettich/restik v0.0.3
	golang.org/x/crypto v0.0.0-20201203163018-be400aefbc4c
)
